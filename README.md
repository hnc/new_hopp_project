# Archive 2021/11/17

This project is not maintained since 2016.

# New hopp Project

Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr <br />
https://gitlab.com/hnc/new_hopp_project

# Project

This project is a toy project using hopp (and aonl). <br />
Clone it to start your C++ project:

```sh
git clone the_repo_of_your_project
cd the_repo_of_your_project
git remote add new_hopp_project https://gitlab.com/hnc/new_hopp_project.git
git remote -v
git pull new_hopp_project master
# git remote remove new_hopp_project
# git remote -v
```

### System Requirement

Required:
- C++11 compiler
- hopp

Optional:
- aonl

### Compilation

###### With CMake

```sh
mkdir build
cd build
cmake ..
make
# make test
./project
```

###### Without CMake

hopp (and aonl) is a header-only library, you can specify where are the include directory of hopp to your IDE. (But you have to define some macros to enable optional parts.)
