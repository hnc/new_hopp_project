# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

https://gitlab.com/hnc/new_hopp_project


 ---------
| Project |
 ---------

This project is a toy project using hopp (and aonl).
Clone it to start your C++ project:

git clone the_repo_of_your_project
cd the_repo_of_your_project
git remote add new_hopp_project https://gitlab.com/hnc/new_hopp_project.git
git remote -v
git pull new_hopp_project master
# git remote remove new_hopp_project
# git remote -v


 --------------------
| System Requirement |
 --------------------

Required:
- hopp

Optional:
- aonl


 -------------
| Compilation |
 -------------

With CMake
----------

mkdir build
cd build
cmake ..
make
# make test
./project

Without CMake
-------------

hopp (and aonl) is a header-only library, you can specify where are the include directory of hopp to your IDE. (But you have to define some macros to enable optional parts.)
